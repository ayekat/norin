#!/bin/sh -

set -euf

# Error codes:
readonly E_SUCCESS=0
readonly E_USER=1
readonly E_INTERNAL=13
readonly E_PERMISSION=127

# Log levels:
#readonly LOG_FATAL=1
readonly LOG_ERROR=2
readonly LOG_WARN=3
readonly LOG_NORMAL=4
readonly LOG_INFO=5
readonly LOG_VERBOSE=6
readonly LOG_DEBUG=7

# TTY colours:
#readonly TTY_FG_BLACK=$(tput setaf 0)
readonly TTY_FG_RED=$(tput setaf 1)
#readonly TTY_FG_GREEN=$(tput setaf 2)
readonly TTY_FG_YELLOW=$(tput setaf 3)
readonly TTY_FG_BLUE=$(tput setaf 4)
#readonly TTY_FG_MAGENTA=$(tput setaf 5)
#readonly TTY_FG_CYAN=$(tput setaf 6)
#readonly TTY_FG_WHITE=$(tput setaf 7)
readonly TTY_RESET=$(printf '\033[0m')

# Paths:
readonly VAULT_PATH=/var/data/snapshots
readonly CONFIG_DIR=/etc/norin
readonly PROFILES_DIR=$CONFIG_DIR/profiles

# Default parameters:
ssh_user=norin
ssh_port=22
host_data=/
rsync_path=/usr/bin/rsync
sudo='sudo '
os_family=linux

logprint()
{
	# Don't print stuff above the configured logging level:
	if [ $(($1)) -gt $((LOG_LEVEL)) ]; then
		return
	fi

	_log_level=$1; shift

	# Message prefix and suffix:
	_log_prefix=
	_log_suffix=
	case $_log_level in
		("$LOG_ERROR") _log_prefix="${TTY_FG_RED}ERROR$TTY_RESET: " ;;
		("$LOG_WARN") _log_prefix="${TTY_FG_YELLOW}WARNING$TTY_RESET: " ;;
		("$LOG_DEBUG")
			_log_prefix="${TTY_FG_BLUE}DEBUG: "
			_log_suffix="$TTY_RESET"
			;;
	esac

	# Set output:
	case $_log_level in
		("$LOG_ERROR"|"$LOG_WARN") exec 3<&2 ;;
		(*) exec 3<&1 ;;
	esac
	{
		printf '%s' "$_log_prefix"
		# shellcheck disable=SC2059
		printf "$@"
		printf '%s\n' "$_log_suffix"
	} >&3
	exec 3<&-

	unset _log_level _log_prefix _log_suffix
}

error()   { logprint $LOG_ERROR   "$@"; }
warn()    { logprint $LOG_WARN    "$@"; }
normal()  { logprint $LOG_NORMAL  "$@"; }
info()    { logprint $LOG_INFO    "$@"; }
verbose() { logprint $LOG_VERBOSE "$@"; }
debug()   { logprint $LOG_DEBUG   "$@"; }

die()
{
	retval=$(($1)); shift
	{
		# shellcheck disable=SC2059
		printf "$@"; echo
		if [ $retval -eq $E_USER ]; then
			printf 'Run with -h for help.\n'
		fi
	} >&2
	exit $retval
}

print_help()
{
	cat <<- EOF
	$0: Take a snapshot of a system

	Usage: $0 [OPTION ...] HOST

	Options:
	  -h     Show this help message and exit
	  -q     Reduce log level by 1 (less verbose). See below for more details.
	  -v     Increase log level by 1 (more verbose). See below for more details.

	Log level:
	  By default, the log level is $LOG_NORMAL (normal output). The log level can be
	  increased or decreased by passing -q or -v one or multiple times. Note
	  that abort messages are always displayed, no matter how many times -q is
	  passed.
	EOF
}

# Options:
LOG_LEVEL=$LOG_NORMAL
while getopts :hqv opt; do
	case "$opt" in
		(h) print_help; exit $E_SUCCESS ;;
		(q) LOG_LEVEL=$((LOG_LEVEL - 1)) ;;
		(v) LOG_LEVEL=$((LOG_LEVEL + 1)) ;;
		(:) die $E_USER 'Missing argument for -%s' "$OPTARG" ;;
		('?') die $E_USER 'Unknown option -%s' "$OPTARG" ;;
		(*) die $E_INTERNAL 'Unexpected option -%s' "$OPTARG" ;;
	esac
done
shift $((OPTIND - 1))
unset OPTARG
readonly LOG_LEVEL

# Positional arguments:
test $# -gt 0 || die $E_USER 'Missing argument: host'
host=$1; shift
test $# -eq 0 || die $E_USER 'Trailing arguments: %s' "$*"

# Permissions:
test "$(id -u)" = '0' || die $E_PERMISSION 'Please run as root'

# Host-specific settings:
hostname="$host"
ssh_key=/var/lib/norin/.ssh/id_ed25519
exclude='/boot /dev /media /mnt /proc /run /sys /tmp /usr /var/cache /var/lib/pacman/sync /var/tmp'
if [ -r "$PROFILES_DIR/$host" ]; then
	# shellcheck disable=SC1090
	. "$PROFILES_DIR/$host"
else
	die $E_USER 'No profile found in %s for host %s' "$PROFILES_DIR" "$host"
fi

# Don't invoke sudo remotely if we already log in as root:
if [ "$ssh_user" = 'root' ]; then
	sudo=
fi

# rsync arguments (--xattrs only added for Linux systems):
set -- --verbose --human-readable
set -- "$@" --recursive --links --perms --times --owner --group
set -- "$@" --sparse --hard-links --numeric-ids --delete --delete-excluded
if [ "$os_family" = 'linux' ]; then
	set -- "$@" --xattrs
fi
for x in $exclude; do
	set -- "$@" --exclude="$x"
done

# Target directory:
host_dir="$VAULT_PATH/$host"
if [ ! -d "$host_dir" ]; then
	info 'First run for %s; creating %s' "$host" "$host_dir"
	mkdir "$host_dir"
fi

# subvolume:
target_dir="$host_dir/current"
if [ ! -d "$target_dir" ]; then
	info 'First run for %s; creating %s' "$host" "$target_dir"
	btrfs subvolume create "$target_dir"
fi

# rsync:
info 'Starting rsync'
debug '%s ' rsync \
	"$@" \
	--rsh="ssh -p $ssh_port -i $ssh_key" \
	--rsync-path="$sudo$rsync_path" \
	"$ssh_user@$hostname:$host_data" "$target_dir"
rsync \
	"$@" \
	--rsh="ssh -p $ssh_port -i $ssh_key" \
	--rsync-path="$sudo$rsync_path" \
	"$ssh_user@$hostname:$host_data" "$target_dir"

# create snapshot:
snapshot="$(date +%Y-%m-%dT%H:%M:%S%:z)"
info 'Creating snapshot %s' "$snapshot"
btrfs subvolume snapshot -r "$target_dir" "$host_dir/$snapshot"
