#!/bin/sh -

set -e

# Error codes:
E_SUCCESS=0
E_USER=1
E_DEVICE=2
E_INTERNAL=13
E_PERMISSION=127

# Colours:
#BLACK=0
RED=1
GREEN=2
YELLOW=3
BLUE=4
MAGENTA=5
#CYAN=6
#WHITE=7

error()   { _print $RED     "$@" >&2; }
warn()    { _print $YELLOW  "$@" >&2; }
info()    { _print $BLUE    "$@";     }
success() { _print $GREEN   "$@";     }
debug()   { _print $MAGENTA "$@";     }
_print()
{
	colour=$(($1 + 30)); shift
	printf '\033[%dm>>>\033[0m ' $colour
	printf "$@"
	echo
}

die()
{
	retval=$(($1)); shift
	error "$@"
	exit $retval
}

print_help()
{
	cat <<- EOF
	$0: Back up NAS to an external USB hard drive

	Usage: $0 [OPTION ...]

	Options:
	  -h  Show this help message and exit
	EOF
}

# Directories:
readonly NAS_PATH=/var/data/nas
readonly SNAPSHOTS_PATH=/var/data/snapshots
readonly OFFSITE_CRYPTNODE=offsite
readonly OFFSITE_CRYPTNODE_PATH=/dev/mapper/$OFFSITE_CRYPTNODE
readonly OFFSITE_MOUNT_PATH=/mnt/$OFFSITE_CRYPTNODE
OFFSITE_UUIDS=
OFFSITE_UUIDS="$OFFSITE_UUIDS d9a25f60-d231-43e3-862c-d8ef5ad59faa" # 2 TB Intenso
OFFSITE_UUIDS="$OFFSITE_UUIDS 906b5638-fd04-49a9-81a7-571056ea98a3" # 1.5 TB WD
OFFSITE_UUIDS="$OFFSITE_UUIDS a7eccbed-c78f-4515-b660-6e6457ee1a32" # 3 TB WD (A)
OFFSITE_UUIDS="$OFFSITE_UUIDS c51140f9-ca5c-4abd-9357-b76bc0d2b0a8" # 3 TB WD (B)
OFFSITE_UUIDS="$OFFSITE_UUIDS 43d0ee1a-b19e-43d5-a7a7-add4dcbe1183" # 3 TB WD (C)
readonly OFFSITE_UUIDS

# Hosts:
readonly HOSTS='brombeeri chirschi gurke heidubeeri kowalski mirabelle pflume'

# Arguments:
if [ $# -gt 0 ]; then
	if [ "$1" = '-h' ]; then
		print_help
		exit $E_SUCCESS
	else
		die $E_USER 'Trailing arguments: %s' "$*"
	fi
fi

# Permissions:
test "$(id -u)" = '0' || die $E_PERMISSION 'Please run as root'

# Read configuration:
if [ -e /etc/norin/norin.cfg ]; then
	. /etc/norin/norin.cfg
fi

# Find device:
for OFFSITE_NODE in $OFFSITE_UUIDS; do
	OFFSITE_NODE_PATH=/dev/disk/by-uuid/$OFFSITE_NODE
	if [ -b "$OFFSITE_NODE_PATH" ]; then
		break
	fi
	warn 'Device %s is not attached' "$OFFSITE_NODE"
	OFFSITE_NODE_PATH=
done
test -n "$OFFSITE_NODE_PATH" || die $E_DEVICE 'No offsite device attached'

readonly OFFSITE_NODE OFFSITE_NODE_PATH

# Open (with keyfile, if existing):
if [ -n "${OFFSITE_CRYPT_KEYFILE:-}" ]
&& [ -e "$OFFSITE_CRYPT_KEYFILE" ]; then
	set -- --key-file "$OFFSITE_CRYPT_KEYFILE"
fi
info 'Opening %s as %s' "$OFFSITE_NODE" "$OFFSITE_CRYPTNODE"
cryptsetup open "$@" "$OFFSITE_NODE_PATH" "$OFFSITE_CRYPTNODE"

# Mount:
info 'Mounting %s to %s' "$OFFSITE_CRYPTNODE" "$OFFSITE_MOUNT_PATH"
mount "$OFFSITE_CRYPTNODE_PATH" "$OFFSITE_MOUNT_PATH"

# Check space:
info 'Calculating available space'
space_avail=$(df --output=size "$OFFSITE_MOUNT_PATH" | tail -n +2)
debug 'Available: %7dM' $((space_avail / 1024))

info 'Calculating required space for data'
space_req_data=$(du -ks "$NAS_PATH" | cut -f1)
debug 'Required:  %7dM' $((space_req_data / 1024))

info 'Calculating required space for snapshots'
space_req_snapshots=0
for snapshot in "$SNAPSHOTS_PATH"/*; do
	[ -d "$snapshot"/current ] || continue
	space_req=$(du -ks "$snapshot/current" | { read -r space _ && echo $space; })
	debug ' %7dM (%s)' $((space_req / 1024)) "$snapshot"
	space_req_snapshots=$((space_req_snapshots + space_req))
done
debug 'Required:  %7dM' $((space_req_snapshots / 1024))

for s in "$space_avail" "$space_req_data" "$space_req_snapshots"; do
	test -n "$s" || die $E_INTERNAL 'Empty space calculated'
	case "$s" in (*[!0-9]*)
		die $E_INTERNAL 'Invalid space calculated: %s' "$s"
	esac
done
space_avail=$((space_avail))
space_req=$((space_req_data + space_req_snapshots))
success 'Offsite requires %d KiB (%d KiB available)' $space_req $space_avail
test $space_avail -ge $space_req || die $E_DEVICE 'Not enough space'

# rsync args:
set -- --verbose --human-readable
set -- "$@" --recursive --links --perms --times --owner --group
set -- "$@" --sparse --hard-links --xattrs --numeric-ids --delete

# rsync (nas):
info 'Backing up %s' "$NAS_PATH"
mkdir -p "$OFFSITE_MOUNT_PATH/nas"
rsync "$@" "$NAS_PATH/" "$OFFSITE_MOUNT_PATH/nas/"

# rsync (snapshots):
for host in $HOSTS; do
	info 'Backing up %s snapshot' "$host"
	mkdir -p "$OFFSITE_MOUNT_PATH/snapshots/$host"
	rsync "$@" "$SNAPSHOTS_PATH/$host/current/" "$OFFSITE_MOUNT_PATH/snapshots/$host/"
done

# Unmount:
info 'Unmounting %s from %s' "$OFFSITE_CRYPTNODE" "$OFFSITE_MOUNT_PATH"
umount "$OFFSITE_MOUNT_PATH"

# Close:
info 'Closing %s as %s' "$OFFSITE_NODE" "$OFFSITE_CRYPTNODE"
cryptsetup close "$OFFSITE_CRYPTNODE"
